export interface UserRaw {
    uid:string;
    id:string,
    name:string,
    username:string,
    email:string,
    phone:string,
    website:string,
    company:{
        name:string,
        catchPhrase:string,
        bs:string
    }
}
