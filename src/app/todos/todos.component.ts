import { Observable } from 'rxjs';
import { AuthService } from './../auth.service';
import { UserService } from './../user.service';
import { Component, OnInit } from '@angular/core';
import { ThrowStmt } from '@angular/compiler';

@Component({
  selector: 'app-todos',
  templateUrl: './todos.component.html',
  styleUrls: ['./todos.component.css']
})
export class TodosComponent implements OnInit {

  constructor(public auth: AuthService, private userservice: UserService) { }

  todostatus:string ="Loading...";
  todos$: Observable<any[]>;
  userId: string;

  done(todoId:string)
  {
    this.userservice.updateTodo(this.userId,todoId);
  }
  ngOnInit() {
    this.auth.getUser().subscribe(
      user => {
        this.userId = user.uid;
        this.todos$ = this.userservice.getTodos(this.userId);
      }
    )
  }
}

