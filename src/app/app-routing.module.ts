import { TodoformComponent } from './todoform/todoform.component';
import { UsersComponent } from './users/users.component';
import { WelcomeComponent } from './welcome/welcome.component';
import { LoginComponent } from './login/login.component';
import { SignUpComponent } from './signup/signup.component';
import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { MyUsersComponent } from './my-users/my-users.component';
import { TodosComponent } from './todos/todos.component';

const appRoutes: Routes = [
    { path: 'welcome', component: WelcomeComponent },
    { path: 'signup', component: SignUpComponent},
    { path: 'login', component: LoginComponent},
    { path: 'users', component: UsersComponent},
    { path: 'myusers', component: MyUsersComponent},
    { path: 'todos', component: TodosComponent},
    { path: 'todoform', component: TodoformComponent},
    { path: '',
      redirectTo: '/welcome',
      pathMatch: 'full'
    },
  ];

  
@NgModule({
  imports: [
    RouterModule.forRoot(appRoutes)
  ],
  exports: [RouterModule]
})

export class AppRoutingModule { }