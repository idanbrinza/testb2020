import { Component, OnInit } from '@angular/core';
import { UserRaw } from '../interface/user-raw';
import { UserService } from '../user.service';

@Component({
  selector: 'app-my-users',
  templateUrl: './my-users.component.html',
  styleUrls: ['./my-users.component.css']
})
export class MyUsersComponent implements OnInit {

  constructor(private userservice: UserService,) { }

  Users$: UserRaw[];
  
  ngOnInit() {
    this.userservice.myUsers()
    .subscribe(data =>this.Users$ = data );
  }

  
}
