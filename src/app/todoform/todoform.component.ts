import { UserService } from './../user.service';
import { Component, OnInit } from '@angular/core';
import { AuthService } from '../auth.service';
import { Router, ActivatedRoute } from '@angular/router';

@Component({
  selector: 'app-todoform',
  templateUrl: './todoform.component.html',
  styleUrls: ['./todoform.component.css']
})
export class TodoformComponent implements OnInit {

  userId:string;
  buttonText:string = 'Add' 
  title:string;

  constructor(private userservice:UserService,
    private authService:AuthService, 
    private router:Router,
    private route: ActivatedRoute) { }

  onSubmit(){ 
    
      console.log('In onSubmit');
      this.userservice.addTodo(this.userId,this.title);
    
    this.router.navigate(['/todos']);  
  } 


  ngOnInit() {
    this.authService.user.subscribe(user => {
      if (user)
        this.userId = user.uid;
    })
  }

}
