import { Injectable } from '@angular/core';
import { HttpClient, HttpErrorResponse } from '@angular/common/http';
import { AngularFirestore, AngularFirestoreCollection } from '@angular/fire/firestore';
import { User } from './interface/user';
import { UserRaw } from './interface/user-raw';
import { Router } from '@angular/router';
import { Observable } from 'rxjs';
import { map } from 'rxjs/operators';

@Injectable({
  providedIn: 'root'
})
export class UserService {

  userapi = "https://jsonplaceholder.typicode.com/users"
  
  constructor(private router: Router, private _http: HttpClient, private database:AngularFirestore) { }

  userCollection:AngularFirestoreCollection = this.database.collection('users');
  todoCollection:AngularFirestoreCollection;
  
  
  getUser(){
    return this._http.get<UserRaw[]>(this.userapi);
  }

  addUser(userId:string, user:UserRaw){
    console.log('adding user');
    const userToSave = {
      uid:userId,
      id:user.id,
      name:user.name,
      username:user.username,
      email:user.email,
      phone:user.phone,
      website:user.website,
      company:{
          name:user.company.name,
          catchPhrase:user.company.catchPhrase,
          bs:user.company.bs
      }
    }
    console.log(userToSave);
    this.userCollection.doc(userId).set(userToSave);
  } 

  myUsers(): Observable<any[]> {
    console.log('User collection created');
    return this.userCollection.snapshotChanges().pipe(
      map(actions => actions.map(a => {
        const data = a.payload.doc.data();
        data.id = a.payload.doc.id;
        return { ...data };
      }))
    );
  }


  getTodos(userId): Observable<any[]> {
    this.todoCollection = this.database.collection(`users/${userId}/todos`);
    console.log('Todo collection created');
    return this.todoCollection.snapshotChanges().pipe(
      map(actions => actions.map(a => {
        const data = a.payload.doc.data();
        data.id = a.payload.doc.id;
        return { ...data };
      }))
    );
  }

  
  addTodo(userId:string, title:string){
    console.log('adding todo');
    const todo = {title:title, status:0}
    this.userCollection.doc(userId).collection('todos').add(todo);
  } 


updateTodo(userId:string, todoId:string){
  this.database.doc(`users/${userId}/todos/${todoId}`).update(
    {
      status:1
    }
  )
}
}