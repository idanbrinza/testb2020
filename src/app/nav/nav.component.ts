import { AuthService } from './../auth.service';
import { Component } from '@angular/core';
import { BreakpointObserver, Breakpoints } from '@angular/cdk/layout';
import { Observable } from 'rxjs';
import { map, shareReplay } from 'rxjs/operators';
import { Location } from "@angular/common";
import { Router } from "@angular/router";

@Component({
  selector: 'app-nav',
  templateUrl: './nav.component.html',
  styleUrls: ['./nav.component.css']
})
export class NavComponent {
  title: string = 'Test';
  userEmail: String;
  userId: string;
  isTest: boolean=false;

  isHandset$: Observable<boolean> = this.breakpointObserver.observe(Breakpoints.Handset)
    .pipe(
      map(result => result.matches),
      shareReplay()
    );


  constructor(private breakpointObserver: BreakpointObserver,
    public authService: AuthService,
    location: Location,
    router: Router) {

    this.authService.user.subscribe(user => {
      if (user) {
        this.userEmail = user.email;
        this.userId = user.uid; 
      if(this.userEmail == "test@test.com")
      this.isTest=true;
      else
      this.isTest=false;
      
      }
    })

    router.events.subscribe(val => {
      if (location.path() == "/welcome") {
        this.title = 'Welcome';
      }
      if (location.path() == "/users") {
        this.title = 'Available Users';
      }
      if (location.path() == "/myusers") {
        this.title = 'Registered Users';
      }
      if (location.path() == "/todos") {
        this.title = 'My Todo List';
      }
      if (location.path() == "/signup") {
        this.title = 'Sign Up';
      }
      if (location.path() == "/login") {
        this.title = 'Login';
      }
    });
  }
}

