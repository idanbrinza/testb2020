import { AuthService } from './../auth.service';
import { UserRaw } from './../interface/user-raw';
import { UserService } from './../user.service';
import { Component, OnInit } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';

@Component({
  selector: 'app-users',
  templateUrl: './users.component.html',
  styleUrls: ['./users.component.css']
})
export class UsersComponent implements OnInit {

  userId:string;

  constructor(private router: Router, private route:ActivatedRoute, private userservice: UserService, private auth: AuthService) { 
    this.auth.user.subscribe(user => {
      if (user)
        this.userId = user.uid;
      })
    }


  Users$: UserRaw[];
  createBtn: string = "Create Account"; 
  password:string = "12345678";
  panelOpenState = false;
  
  createAccount(user:UserRaw) {
    this.auth.SignUp(user.email, this.password);
    this.userservice.addUser(this.userId,user);
    this.router.navigate(['/welcome']);
  //  this.createBtn = "Account Created!"; 
  }

  ngOnInit() {
    this.userservice.getUser()
    .subscribe(data =>this.Users$ = data ); 
  }

}
