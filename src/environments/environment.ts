// This file can be replaced during build by using the `fileReplacements` array.
// `ng build --prod` replaces `environment.ts` with `environment.prod.ts`.
// The list of file replacements can be found in `angular.json`.

export const environment = {
  production: false,
  firebase:{
    apiKey: "AIzaSyCTdy-cOQtRRgHIhSuLAmxTNN-UKOsnmQk",
    authDomain: "testb2020-32b11.firebaseapp.com",
    databaseURL: "https://testb2020-32b11.firebaseio.com",
    projectId: "testb2020-32b11",
    storageBucket: "testb2020-32b11.appspot.com",
    messagingSenderId: "759312534238",
    appId: "1:759312534238:web:454e91e3c3dc33bc36904f"
}
};

/*
 * For easier debugging in development mode, you can import the following file
 * to ignore zone related error stack frames such as `zone.run`, `zoneDelegate.invokeTask`.
 *
 * This import should be commented out in production mode because it will have a negative impact
 * on performance if an error is thrown.
 */
// import 'zone.js/dist/zone-error';  // Included with Angular CLI.
